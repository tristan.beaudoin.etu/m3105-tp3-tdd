package main;

import static org.junit.Assert.assertEquals;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class testFooBarQix {
	
	
	@BeforeEach
	public void setup() {
		
	}
	
	@Test
	public void test_return_string() {
		//assertEquals(FBQ.FooBarQix(13),"Foo");
		assertEquals(FBQ.fooBarQix(0),"0");
		assertEquals(FBQ.fooBarQix(1),"1");
	}
	
	@Test
	public void modulo3() {
		assertEquals("Foo",FBQ.fooBarQix(6));
	}
	
	@Test
	public void modulo5() {
		assertEquals("Bar",FBQ.fooBarQix(10));		
	}
	
	@Test
	public void modulo7() {
		assertEquals("Qix",FBQ.fooBarQix(49));
	}
	
	@Test
	public void modulo_contains(){
		assertEquals("FooFoo",FBQ.fooBarQix(3));
		assertEquals("FooBarBar",FBQ.fooBarQix(15));
		assertEquals("BarBar",FBQ.fooBarQix(5));
		assertEquals("BarQixQix",FBQ.fooBarQix(70));
		assertEquals("QixQix",FBQ.fooBarQix(7));
	}
	

	@AfterEach
	public void teardown() {
		
	}

}
